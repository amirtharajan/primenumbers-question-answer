'''Qustion:

Write Python code to find if ALL the numbers in a given list of integers are PART of the series 
defined by the following. f(0) = 0 f(1) = 1 f(n) = 2*f(n-1) - 2*f(n-2) for all n > 1. 
def is_part_of_series(lst):

'''


'''****************************** Solution given by Amirdharajan *************************************

Sample Input output

INPUT : 512, 0, -1024, -2048, -2048, 0, 4096, 8192
OUTPUT : True

INPUT : 513, 0, -1024, -2048, -2048, 0, 4096, 8192
OUTPUT : False

Thank you'''


#main program

from math import ceil

def is_part_of_series(lst):
    if len(lst)==1:
        print('''\n\t************************************************\n
        The given list length is 1, So cannot determine\n
        ************************************************''')
        exit()

    # This loop used to check whether there is any one ODD number in tha list
    # If ODD number in the list, it will terminate the program & return False
    for n in lst:
        if n%2 != 0:
            print('''\n\t*********************************************\n
        {}, 'The given list is NOT IN the series'\n
        *********************************************'''.format(False))
            exit()

    if lst[-1] != 0:
        n = abs(lst[-1])
    else:
        n = abs(lst[-2])

    if lst[-1] == lst[-2]:
        n = ceil(n ** 0.5) + 2
    elif lst[-2] == 0:
        n = ceil(n ** 0.5) + 4
    elif lst[-1] == 0:
        n = ceil(n ** 0.5) + 5
    else:
        n = ceil(n ** 0.5) + 3

    x0 = 0
    x1 = 1
    temp_list = []
    n = n+2
    for i in range(2, n):
        f1 = 2*x1
        f0 = 2*x0
        fx = f1 - f0
        temp_list.append(fx)
        x0 = x1
        x1 = fx

    if(set(lst).issubset(set(temp_list))):
        print('''\n\t***************************************\n
        {}, 'The given list is IN the series'\n
        ***************************************'''.format(True))
    else:
        print('''\n\t******************************************\n
        {}, 'The given list is NOT IN the series'\n
        ******************************************'''.format(False))



# Driver Program

# Complie time sample input 
lst = [512, 0, -1024, -2048, -2048, 0, 4096, 8192]
# lst = list(map(int,input('''\n------------------------ ***Welcome*** ---------------------------\n
# Only Enter the list elements separated by COMMA\n\n''').split(',')))

is_part_of_series(lst)